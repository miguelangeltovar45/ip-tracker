let map;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: {lat: 4.6486259, lng: -74.2478934},
    zoom: 8,
    disableDefaultUI: true,
  });
}

$('#search').click(() => {
  getIPAddresData($('.search input').val())
})

function getIPAddresData(ip) {
  let apiUrl = '';
  if (/\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b/.test(ip)) {
    console.log('ipv4')
    apiUrl = `https://geo.ipify.org/api/v1?apiKey=at_UDAE0JHodbT5cBFMTkynTb0Mw2sPO&ipAddress=${ip}`
  } else if (/\b(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))\b/.test(ip)) {
    apiUrl = `https://geo.ipify.org/api/v1?apiKey=at_UDAE0JHodbT5cBFMTkynTb0Mw2sPO&ipAddress=${ip}`
  } else if (/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n?]+)/img.test(ip)) {
    apiUrl = `https://geo.ipify.org/api/v1?apiKey=at_UDAE0JHodbT5cBFMTkynTb0Mw2sPO&domain=${ip}`
  }
  $.ajax({
    url: apiUrl
  }).done((data) => {
    $('#ip-address').text(data.ip);
    $('#isp').text(data.isp);
    $('#timezone').text(data.location.timezone);
    $('#location').text(`${data.location.country} ${data.location.city}, ${data.location.region} ${data.location.postalCode}`);
    map = new google.maps.Map(document.getElementById("map"), {
      center: {lat: data.location.lat, lng: data.location.lng},
      zoom: 8,
      disableDefaultUI: true,
    });
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(data.location.lat, data.location.lng),
      map: map,
      icon: './images/icon-location.svg'
    });
    google.maps.event.addListener(marker, 'click', () => {
      map.panTo(marker.getPosition());
    })
  }).fail((error) => {
    if (error.responseJSON.code === 422) {
      alert('No es un dominio válido')
    }
  })
}
